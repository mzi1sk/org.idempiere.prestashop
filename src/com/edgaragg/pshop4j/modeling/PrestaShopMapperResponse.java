/**
 * 
 */
package com.edgaragg.pshop4j.modeling;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.edgaragg.pshop4j.pojos.PrestaShopPojo;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author Edgar Gonzalez
 *
 */
public class PrestaShopMapperResponse<T extends PrestaShopPojo> {

	private T resource;
	private String hash;
	private Map<String, List<String>> headers;
	private Exception exception;
	private Document doc;
	private NodeList nList;
	private String XMLSource;
	
	/**
	 * 
	 */
	public PrestaShopMapperResponse() {

	}
	
	protected PrestaShopMapperResponse<T> withResource(T resource){
		this.resource = resource;
		return this;
	}
	
	protected PrestaShopMapperResponse<T> withHeaders(Map<String, List<String>> headers){
		this.headers = headers;
		return this;
	}

	protected PrestaShopMapperResponse<T> withHash(String hash){
		this.hash = hash;
		return this;
	}
	
	protected PrestaShopMapperResponse<T> withDoc(InputStream stream){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
        	builder = factory.newDocumentBuilder();
			this.doc = builder.parse(stream);
			doc.getDocumentElement().normalize();
			this.nList = doc.getElementsByTagName("prestashop");
			
			DOMSource domSource = new DOMSource(doc);
		    StringWriter writer = new StringWriter();
			StreamResult sresult = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer atransformer = tf.newTransformer();
			atransformer.transform(domSource, sresult);
			this.XMLSource=writer.toString();

			
		
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	protected PrestaShopMapperResponse<T> withException(Exception exception){
		this.exception = exception;
		return this;
	}
	
	/**
	 * @return the resource
	 */
	public T getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(T resource) {
		this.resource = resource;
	}

	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}

	/**
	 * @param hash the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}

	/**
	 * @return the headers
	 */
	public Map<String, List<String>> getHeaders() {
		return headers;
	}

	/**
	 * @param headers the headers to set
	 */
	public void setHeaders(Map<String, List<String>> headers) {
		this.headers = headers;
	}

	/**
	 * @return the exception
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(Exception exception) {
		this.exception = exception;
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}

	public NodeList getnList() {
		return nList;
	}

	public void setnList(NodeList nList) {
		this.nList = nList;
	}

	public String getXMLSource() {
		return XMLSource;
	}

	public void setXMLSource(String xMLSource) {
		XMLSource = xMLSource;
	}
	
	

}
