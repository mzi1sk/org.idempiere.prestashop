/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.idempiere.eshop.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.I_Persistent;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.model.POInfo;
import org.compiere.util.Env;
import org.compiere.model.I_AD_Table;

/** Generated Model for MZI_EShop_Mapping
 *  @author iDempiere (generated) 
 *  @version Release 4.1 - $Id$ */
public class X_MZI_EShop_Mapping extends PO implements I_MZI_EShop_Mapping, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171212L;

    /** Standard Constructor */
    public X_MZI_EShop_Mapping (Properties ctx, int MZI_EShop_Mapping_ID, String trxName)
    {
      super (ctx, MZI_EShop_Mapping_ID, trxName);
      /** if (MZI_EShop_Mapping_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_MZI_EShop_Mapping (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_MZI_EShop_Mapping[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Table getAD_Table() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Table)MTable.get(getCtx(), org.compiere.model.I_AD_Table.Table_Name)
			.getPO(getAD_Table_ID(), get_TrxName());	}

	/** Set Table.
		@param AD_Table_ID 
		Database Table information
	  */
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1) 
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else 
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Eshop_Ref_ID.
		@param Eshop_Ref_ID Eshop_Ref_ID	  */
	public void setEshop_Ref_ID (int Eshop_Ref_ID)
	{
		if (Eshop_Ref_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_Eshop_Ref_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Eshop_Ref_ID, Integer.valueOf(Eshop_Ref_ID));
	}

	/** Get Eshop_Ref_ID.
		@return Eshop_Ref_ID	  */
	public int getEshop_Ref_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Eshop_Ref_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Magento = M */
	public static final String EXTERNALSYSTEM_Magento = "M";
	/** XelaPOS = O */
	public static final String EXTERNALSYSTEM_XelaPOS = "O";
	/** SugarCRM = S */
	public static final String EXTERNALSYSTEM_SugarCRM = "S";
	/** General = G */
	public static final String EXTERNALSYSTEM_General = "G";
	/** Stock Current = SC */
	public static final String EXTERNALSYSTEM_StockCurrent = "SC";
	/** Email = E */
	public static final String EXTERNALSYSTEM_Email = "E";
	/** PrestaShop = PS */
	public static final String EXTERNALSYSTEM_PrestaShop = "PS";
	/** Set ExternalSystem.
		@param ExternalSystem ExternalSystem	  */
	public void setExternalSystem (String ExternalSystem)
	{

		set_Value (COLUMNNAME_ExternalSystem, ExternalSystem);
	}

	/** Get ExternalSystem.
		@return ExternalSystem	  */
	public String getExternalSystem () 
	{
		return (String)get_Value(COLUMNNAME_ExternalSystem);
	}

	/** Set MZI_EShop_Mapping_ID.
		@param MZI_EShop_Mapping_ID MZI_EShop_Mapping_ID	  */
	public void setMZI_EShop_Mapping_ID (int MZI_EShop_Mapping_ID)
	{
		if (MZI_EShop_Mapping_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_MZI_EShop_Mapping_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_MZI_EShop_Mapping_ID, Integer.valueOf(MZI_EShop_Mapping_ID));
	}

	/** Get MZI_EShop_Mapping_ID.
		@return MZI_EShop_Mapping_ID	  */
	public int getMZI_EShop_Mapping_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MZI_EShop_Mapping_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Record ID.
		@param Record_ID 
		Direct internal record ID
	  */
	public void setRecord_ID (int Record_ID)
	{
		if (Record_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_Record_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Record_ID, Integer.valueOf(Record_ID));
	}

	/** Get Record ID.
		@return Direct internal record ID
	  */
	public int getRecord_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Record_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_W_Store getW_Store() throws RuntimeException
    {
		return (org.compiere.model.I_W_Store)MTable.get(getCtx(), org.compiere.model.I_W_Store.Table_Name)
			.getPO(getW_Store_ID(), get_TrxName());	}

	/** Set Web Store.
		@param W_Store_ID 
		A Web Store of the Client
	  */
	public void setW_Store_ID (int W_Store_ID)
	{
		if (W_Store_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_W_Store_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_W_Store_ID, Integer.valueOf(W_Store_ID));
	}

	/** Get Web Store.
		@return A Web Store of the Client
	  */
	public int getW_Store_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_W_Store_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}