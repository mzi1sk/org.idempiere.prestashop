package org.idempiere.eshop.wsclient.process;


import java.util.logging.Level;

import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MStore;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.idempiere.eshop.wsclient.synch.SynchAttributes;
import org.idempiere.eshop.wsclient.synch.SynchCategories;
import org.idempiere.eshop.wsclient.synch.SynchImages;
import org.idempiere.eshop.wsclient.synch.SynchOrders;
import org.idempiere.eshop.wsclient.synch.SynchProducts2;
import org.idempiere.eshop.wsclient.synch.SynchStock;

public class PrestashopSync extends SvrProcess {
	private String SynchronizationType = null;
	private int p_W_Store_ID;
	private Boolean reset;
	
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("W_Store_ID"))
				p_W_Store_ID = para[i].getParameterAsInt();
			else if (name.equals("SynchronizationType"))
				SynchronizationType= (String) para[i].getParameter();
			else if (name.equals("Reset"))
				reset=  para[i].getParameterAsBoolean();

			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {

		
		MStore store = new MStore(Env.getCtx(), p_W_Store_ID, get_TrxName());
		
		if (!store.isMaintenanceMode()&reset)
			return "Swith ON Maintenance Mode For The Web Store!";
		
//		if (SynchronizationType.equals("IN"))
//				reset=true;
		
		if (SynchronizationType.equals("IN") || SynchronizationType==null
				|| MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductCategory.equals(SynchronizationType)) {
			statusUpdate("Processing - Synchronizing Categories");
			SynchCategories synccat = new SynchCategories(store,this);
			if (reset) synccat.initialize();
				synccat.run();
		}
		
		if (SynchronizationType.equals("IN")|| SynchronizationType==null
				|| MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductAttribute.equals(SynchronizationType)) {
			SynchAttributes syncattr=new SynchAttributes(store,this);
			if (reset) syncattr.initialize();
			 	syncattr.run();
		}
		
		if (SynchronizationType.equals("IN")|| SynchronizationType==null
				|| MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product.equals(SynchronizationType)) {
			SynchProducts2 syncprod2=new SynchProducts2(store,this);
			if (reset) syncprod2.initialize();
			
			return syncprod2.run();
		}
		
		if (SynchronizationType.equals("IN")|| SynchronizationType==null
				|| MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductImage.equals(SynchronizationType)) {
			SynchImages syncimages=new SynchImages(store,this);
			if (reset) syncimages.initialize();
			
			syncimages.run();
		}
		
		if (SynchronizationType.equals("IN")||SynchronizationType == null
				|| MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_StockCurrent.equals(SynchronizationType)) {
			SynchStock synchstorage=new SynchStock(store,this);
			if (reset) synchstorage.initialize();
			
			synchstorage.run();
		}
		
		
		if (SynchronizationType == null
				|| MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Order.equals(SynchronizationType)) {
			SynchOrders syncorders=new SynchOrders(store,this);
			syncorders.run();
		}

		return "OK";
	}
	

    public void ProcessStatusUpdate(String status) {
    	statusUpdate(status);
    }
    
    
 
}
