/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.transform.TransformerException;

import static java.lang.Math.toIntExact;

import org.compiere.model.MAttribute;
import org.compiere.model.MAttributeValue;
import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MStore;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;
import org.idempiere.eshop.wsclient.process.PrestashopSync;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * @author MZI1SK
 */
public class SynchAttributes extends EShopInitialization {

	private MStore store=null;
	private PSWebServiceClient ws;	
	private Map<Integer ,Integer > mapAttributes = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapAttributeValues = new HashMap<Integer ,Integer >();
	
	private int ps_attribute_id=0;
	/**
	 * synch attributes used in products to prestashop
	 */
	public SynchAttributes(MStore store,PrestashopSync process) {
		
		super(store,process);
		this.store=store;
		if (this.ws==null)
		    this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
	}
	
	public void run() {
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductAttribute,MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, null);
	
		long start =  Calendar.getInstance().getTimeInMillis();
		mapAttributeValues=getMapAttributes();
		mapAttributeValues=getMapAttributeValues(true);
		
		List<MAttribute> attributes=getAttributesToSynch ((synch==null?null:synch.getTimestampLocal()));
		for (MAttribute attribute:attributes){
			addProductAttribute(attribute);
			addProductAttributeValues(attribute);
		}
		
		MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, null);
		synchsucces.setAD_Org_ID(store.getAD_Org_ID());
		synchsucces.setSuccess(true);
		synchsucces.setExternalSystem(MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop);
		synchsucces.setTimestampLocal(new Timestamp(start));
		synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductAttribute);
		synchsucces.setW_Store_ID(store.get_ID());
		synchsucces.saveEx();

		setMapAttributes(mapAttributes);
	}
	
	private void addProductAttribute(MAttribute attribute ){
		
		 try {
			 	HashMap<String,Object> getSchemaOpt = new HashMap<String, Object>();
				getSchemaOpt.put("url", store.getURL()+"/api/product_options?schema=blank");       
				Document schema = ws.get(getSchemaOpt);     
		        schema.getElementsByTagName("is_color_group").item(0).setTextContent("0");
		        schema.getElementsByTagName("group_type").item(0).setTextContent("select");
		        Element name = (Element) schema.getElementsByTagName("name").item(0).getFirstChild();
		        name.appendChild(schema.createCDATASection(attribute.getName()));
		        name.setAttribute("id", "1");
		        Element name2 = (Element) schema.getElementsByTagName("name").item(0).getChildNodes().item(1);
		        name2.appendChild(schema.createCDATASection(attribute.getName()));
		        name2.setAttribute("id", "2");
		        
		        
		        Element public_name = (Element) schema.getElementsByTagName("public_name").item(0).getFirstChild();
		        public_name.appendChild(schema.createCDATASection(attribute.getName()));
		        public_name.setAttribute("id", "1");
		        Element public_name2 = (Element) schema.getElementsByTagName("public_name").item(0).getChildNodes().item(1);
		        public_name2.appendChild(schema.createCDATASection(attribute.getName()));
		        public_name2.setAttribute("id", "2");
		        
		        boolean doPut=false; //Put=update ; post=insert
				// synchronize
		        if (!mapAttributes.isEmpty() && mapAttributes.get(attribute.get_ID())!=null)
					doPut=true;
		        
		        long id = 0;
				
				if (doPut){
			        schema.getElementsByTagName("id").item(0).setTextContent(Integer.toString(mapAttributes.get(attribute.get_ID())));
			        getSchemaOpt.put("id", mapAttributes.get(attribute.get_ID()));
			        getSchemaOpt.put("putXml", ws.DocumentToString(schema));
			        //Document ws_attr = 
			        ws.edit(getSchemaOpt);     
			        id = mapAttributes.get(attribute.get_ID());
			        
				}
				else
				{
					try {
						getSchemaOpt.put("postXml", ws.DocumentToString(schema));
						Document ws_attr = ws.add(getSchemaOpt);     
						id = Integer.valueOf(PSWebServiceClient.getCharacterDataFromElement((Element) ws_attr.getElementsByTagName("id").item(0)));
					} 
					catch (Exception e) {
						process.ProcessStatusUpdate("Attribute added "+attribute.getName()+" error uploading ");
					}
				}
				
		        
			X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
			mapping.setAD_Table_ID(MAttribute.Table_ID);
			mapping.setEshop_Ref_ID(toIntExact(id) );
			mapping.setRecord_ID(attribute.get_ID());
			mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
			mapping.setW_Store_ID(store.get_ID());
			mapping.saveEx();
			
			ps_attribute_id=toIntExact(id);
			mapAttributes.put(attribute.get_ID(), toIntExact(id));
				
		} catch (PrestaShopWebserviceException e) {
			System.out.println(e.getMessage());
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
   }
	
	private void addProductAttributeValues(MAttribute attribute ){
		
		 try {
			 
			 int i=0;
			 for(MAttributeValue attributeValue:attribute.getMAttributeValues() ) {
				 i++;
			 	HashMap<String,Object> getSchemaOpt = new HashMap<String, Object>();
				getSchemaOpt.put("url", store.getURL()+"/api/product_option_values?schema=blank");       
				Document schema = ws.get(getSchemaOpt);     
		        schema.getElementsByTagName("id_attribute_group").item(0).setTextContent(Integer.toString(ps_attribute_id));
		        schema.getElementsByTagName("position").item(0).setTextContent(Integer.toString(i) );
		        Element name = (Element) schema.getElementsByTagName("name").item(0).getFirstChild();
		        name.appendChild(schema.createCDATASection(attributeValue.getValue()));
		        name.setAttribute("id", "1");
		        Element name2 = (Element) schema.getElementsByTagName("name").item(0).getChildNodes().item(1);
		        name2.appendChild(schema.createCDATASection(attributeValue.getValue()));
		        name2.setAttribute("id", "2");
		        
		        boolean doPut=false; //Put=update ; post=insert
				// synchronize
		        if (!mapAttributeValues.isEmpty() && mapAttributeValues.get(attributeValue.get_ID())!=null)
					doPut=true;
		        
		        long id = 0;
				
				if (doPut){
			        schema.getElementsByTagName("id").item(0).setTextContent(Integer.toString(mapAttributeValues.get(attributeValue.get_ID())));
			        getSchemaOpt.put("id", mapAttributeValues.get(attributeValue.get_ID()));
			        getSchemaOpt.put("putXml", ws.DocumentToString(schema));
			        //Document ws_attr = 
			        ws.edit(getSchemaOpt);     
			        id = mapAttributeValues.get(attributeValue.get_ID());
			        
				}
				else
				{
					try {
						getSchemaOpt.put("postXml", ws.DocumentToString(schema));
						Document ws_attr = ws.add(getSchemaOpt);     
						id = Integer.valueOf(PSWebServiceClient.getCharacterDataFromElement((Element) ws_attr.getElementsByTagName("id").item(0)));
						process.ProcessStatusUpdate("Attribute added "+attributeValue.getName()+" "+id);
					} 
					catch (Exception e) {
						System.out.println("Attribute added "+attributeValue.getName()+" error uploading ");
					}
				}
					        
				X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
				mapping.setAD_Table_ID(MAttributeValue.Table_ID);
				mapping.setEshop_Ref_ID(toIntExact(id) );
				mapping.setRecord_ID(attributeValue.get_ID());
				mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
				mapping.setW_Store_ID(store.get_ID());
				mapping.saveEx();
				mapAttributeValues.put(attribute.get_ID(), toIntExact(id));
					
			 }//for 
		} catch (PrestaShopWebserviceException e) {
			// TODO Auto-generated catch block
		 	// e.printStackTrace();
			System.out.println(e.getMessage());
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
  }
	
	public void initialize() {
		try {
			HashMap<String,Object> product_attributes = new HashMap<String, Object>();
			product_attributes.put("resource", "product_option_values/");
			Document ws_product_attributes;
			ws_product_attributes = ws.get(product_attributes);
		 
			//System.out.println(ws.DocumentToString(ws_product_attributes));
			if (ws_product_attributes.getElementsByTagName("product_option_value").getLength()>0){
				for (int i=0;i<ws_product_attributes.getElementsByTagName("product_option_value").getLength();i++){
					Node attribute= ws_product_attributes.getElementsByTagName("product_option_value").item(i);
					NamedNodeMap attrs = attribute.getAttributes();
					String attribute_id = attrs.getNamedItem("id").getNodeValue();
					HashMap<String,Object> product_attribute_todel = new HashMap<String, Object>();
					product_attribute_todel.put("resource", "product_option_values/"+attribute_id);
					product_attribute_todel.put("id",attribute_id);
	
					int maxAttempts= 3;
					for (int count = 0; count < maxAttempts; count++) {
						
						PSWebServiceClient ws_del;
						try {
						ws_del =new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
						Boolean deleted= ws_del.delete(product_attribute_todel);
						if (!deleted)
							continue;
						process.ProcessStatusUpdate("product attribute id."+attribute_id+ " deleted ");
						} catch (PrestaShopWebserviceException e) {
							System.out.println("product attribute id."+attribute_id+ " NOT deleted ");
						}
						count = maxAttempts;
						
					}
				}
	        
	        };   
	        
	        
	    	product_attributes = new HashMap<String, Object>();
			product_attributes.put("resource", "product_options/");
			ws_product_attributes = ws.get(product_attributes);
		 
			//System.out.println(ws.DocumentToString(ws_product_attributes));
			if (ws_product_attributes.getElementsByTagName("product_option").getLength()>0){
				for (int i=0;i<ws_product_attributes.getElementsByTagName("product_option").getLength();i++){
					Node attribute= ws_product_attributes.getElementsByTagName("product_option").item(i);
					NamedNodeMap attrs = attribute.getAttributes();
					String attribute_id = attrs.getNamedItem("id").getNodeValue();
					HashMap<String,Object> product_attribute_todel = new HashMap<String, Object>();
					product_attribute_todel.put("resource", "product_options/"+attribute_id);
					product_attribute_todel.put("id",attribute_id);
	
					int maxAttempts= 3;
					for (int count = 0; count < maxAttempts; count++) {
		
						PSWebServiceClient ws_del;
						try {
						ws_del =new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
						Boolean deleted= ws_del.delete(product_attribute_todel);
						if (!deleted)
							continue;
						process.ProcessStatusUpdate("product attribute id."+attribute_id+ " deleted "+deleted);
						} catch (PrestaShopWebserviceException e) {
							System.out.println("product attribute id."+attribute_id+ " NOT deleted ");
						}
						count = maxAttempts;
						
					}
				}
	        
	        };   
		} catch (PrestaShopWebserviceException e) {
		}
		DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MAttribute.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
		DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MAttributeValue.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
		DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductAttribute+"' AND W_Store_ID="+store.get_ID(),null);
	}
		
		
	

	private List<MAttribute> getAttributesToSynch(Timestamp lastsuccess ) {
		if (lastsuccess==null)
			lastsuccess= new Timestamp(0);
		List<MAttribute> categories=new Query(Env.getCtx(), MAttribute.Table_Name, " Updated>? AND m_attribute_id in "
				+ " ( select au.m_attribute_id from m_attributeset at " + 
				"		join m_attributeuse au on at.m_attributeset_id=au.m_attributeset_id " + 
				"		join m_product p on p.m_attributeset_id=at.m_attributeset_id \n" + 
				"		where p.M_Product_ID in "
				+ "	 ( select ci.record_id from  C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id " 
				+ " where cl.C_Vocabulary_ID in (select C_Vocabulary_ID from W_StoreVocabulary where w_store_id=?)))" , null)
				.setParameters(lastsuccess, store.getW_Store_ID())
				.setClient_ID()
				.setOrderBy(MAttribute.COLUMNNAME_Name)
				.list();
		return categories;
	}

}
