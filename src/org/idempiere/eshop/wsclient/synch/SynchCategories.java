/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import static java.lang.Math.toIntExact;

import org.compiere.model.MClassification;
import org.compiere.model.MImage;
import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MProductCategory;
import org.compiere.model.MStore;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;
import org.idempiere.eshop.wsclient.process.PrestashopSync;

import com.edgaragg.pshop4j.model.Limit;
import com.edgaragg.pshop4j.model.Sort;
import com.edgaragg.pshop4j.modeling.PrestaShopMapperResponse;
import com.edgaragg.pshop4j.modeling.enums.PShopBoolean;
import com.edgaragg.pshop4j.pojos.PrestaShopPojoEntity;
import com.edgaragg.pshop4j.pojos.entities.Category;
import com.edgaragg.pshop4j.pojos.entities.LanguageElement;
import com.edgaragg.pshop4j.pojos.list.Categories;
import com.edgaragg.pshop4j.pojos.list.LanguageElements;



/**
 * @author Edgar Gonzalez
 *
 */
public class SynchCategories extends EShopInitialization {

	private PSWebServiceClient ws;	
	private MStore store=null;
	private Map<Integer ,Integer > mapCategories = new HashMap<Integer ,Integer >();
	
	/**
	 * 
	 */
	public SynchCategories(MStore store,PrestashopSync process) {
		
		super(store,process);
		this.store=store;
		if (this.ws==null)
		    this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);

	}
	
	public void run() {
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductCategory,MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, null);
		
		long start = Calendar.getInstance().getTimeInMillis();
		mapCategories=getMapCategories();
		
		List<MClassification> categories=getCategoriesToSynch((synch==null?null:synch.getTimestampLocal()));
		for (MClassification cat:categories){
			
			Category entity = new Category();

			if (cat.isActive())
				entity.setActive(PShopBoolean.TRUE);
			else 
				entity.setActive(PShopBoolean.FALSE);
			
			entity.setDescription(new LanguageElements());
			entity.getDescription().add(new LanguageElement().withId(1).withContent(cat.getDescription()));
			entity.getDescription().add(new LanguageElement().withId(2).withContent(cat.getDescription()));
			entity.setLinkRewrite(new LanguageElements());
			entity.getLinkRewrite().add(new LanguageElement().withId(1).withContent(cat.getName().toLowerCase().replaceAll("[^A-Za-z0-9\\s]", "").replace(" ","-")));
			entity.getLinkRewrite().add(new LanguageElement().withId(2).withContent(cat.getName().toLowerCase().replaceAll("[^A-Za-z0-9\\s]", "").replace(" ","-")));
			entity.setName(new LanguageElements());
			entity.getName().add(new LanguageElement().withId(1).withContent(cat.getName()));
			entity.getName().add(new LanguageElement().withId(2).withContent(cat.getName()));
			
			if (!mapCategories.isEmpty() && mapCategories.get(cat.getC_ParentClassification_ID())!=null && mapCategories.get(cat.getC_ParentClassification_ID())>0)
				entity.setIdParent(mapCategories.get(cat.getC_ParentClassification_ID()));
			else
				entity.setIdParent(2);
				//entity.setIsRootCategory(PShopBoolean.TRUE);
			
			PrestaShopMapperResponse<Category> result =null;
			
			boolean doPut=false;
			if (!mapCategories.isEmpty() && mapCategories.get(cat.get_ID())!=null)
				doPut=true;
			
			if (doPut){
				entity.setId(mapCategories.get(cat.get_ID()));
				result = this.getMapper().put(entity);
			}
			else 
				 result = this.getMapper().post(entity);
			
			if(result.getException()!=null)
				System.out.println(result.getException());
			Category resource = result.getResource();
			long id = resource.getId();
			if (cat.getImage_ID()>0)
			{
				MImage image=new MImage(Env.getCtx(),cat.getImage_ID(),null);
				addImage(image,toIntExact(id));
			}
			 
			
			if (!doPut){
			X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
			mapping.setAD_Table_ID(MProductCategory.Table_ID);
			mapping.setEshop_Ref_ID(toIntExact(id) );
			mapping.setRecord_ID(cat.get_ID());
			mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
			mapping.setW_Store_ID(store.get_ID());
			mapping.saveEx();
			mapCategories.put(cat.get_ID(), toIntExact(id));
			}
			long end = Calendar.getInstance().getTimeInMillis();
			System.out.printf("Categories - ("+(doPut?"PUT":"POST")+") - Execution time: %.2f seconds\n" , (end - start)/1000.0);
			
		}
		
		MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, null);
		synchsucces.setAD_Org_ID(store.getAD_Org_ID());
		synchsucces.setSuccess(true);
		synchsucces.setExternalSystem(MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop);
		synchsucces.setTimestampLocal(new Timestamp(start));
		synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductCategory);
		synchsucces.setW_Store_ID(store.get_ID());
		synchsucces.saveEx();

		setMapCategories(mapCategories);
	}
	
	private void addImage(MImage image,int ps_categoryId ){
		
		
		 if (image.getData()==null) 
			 return ;
		 
		 try {
			 
			 // check size of a image if high do resize
			 ByteArrayOutputStream os = new ByteArrayOutputStream();
			 Image img=image.getImage();
			 if (img==null) 
				 return ;
			 
			 BufferedImage bi=PSWebServiceClient.toBufferedImage(img);
//			 if ( img.getWidth(null)>1280){
//				 System.out.println("image width:"+img.getWidth(null)+"  image height:"+ img.getHeight(null));
//				 int type = bi.getType() == 0? BufferedImage.TYPE_INT_ARGB : bi.getType();
//				//= resizeImageWithHint(bi,type);
//			 }
//			 
			 ImageIO.write(bi,"png", os); 
			 os.flush();
			 InputStream fis = new ByteArrayInputStream(os.toByteArray());
			
			 // synchronize
			 this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
			 //Document ws_image = 
			 ws.addImgProduct(fis, ps_categoryId,image.getName(),"/api/images/categories/",true);
			 
				
		} catch (IOException e) {
			// TODO Auto-generated catch block
		 	// e.printStackTrace();
			System.out.println(e.getMessage());
		}
	 
   }

	public void initialize() {
		long start = Calendar.getInstance().getTimeInMillis();
		
		try {
			List<String> fields = Collections.emptyList();
			PrestaShopMapperResponse<Categories> result = this.getMapper().list(Categories.class, fields, this.getFilters(), Sort.EMPTY_SORT, Limit.EMPTY_LIMIT);
			Categories resource = result.getResource();
			for (int i=2;i<resource.size();i++){
				Category rst = resource.get(i);
				PrestaShopPojoEntity entity=(PrestaShopPojoEntity) rst;
				this.getMapper().delete(entity);
				System.out.println("Category deleted ID: " + rst.getId());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MProductCategory.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
		DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductCategory+"' AND W_Store_ID="+store.get_ID(),null);
		long end = Calendar.getInstance().getTimeInMillis();
		System.out.printf("Products - testGetOnlyID - Execution time: %.5f seconds\n", (end - start)/1000.0);
	}

	private List<MClassification> getCategoriesToSynch(Timestamp lastsuccess ) {
		if (lastsuccess==null)
			lastsuccess= new Timestamp(0);
		List<MClassification> categories=new Query(Env.getCtx(), MClassification.Table_Name, " Updated>? AND  C_Vocabulary_ID in (select C_Vocabulary_ID from W_StoreVocabulary where w_store_id=?) ", null)
				.setParameters(lastsuccess, store.getW_Store_ID())
				.setClient_ID()
				.setOrderBy(MClassification.COLUMNNAME_C_Classification_ID)
				.list();
	
		return categories;
	}
	
}
