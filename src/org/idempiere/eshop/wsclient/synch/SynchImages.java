/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;



import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.xml.transform.TransformerException;

import static java.lang.Math.toIntExact;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.compiere.model.MImage;
import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MProduct;
import org.compiere.model.MStore;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;
import org.idempiere.eshop.wsclient.process.PrestashopSync;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * @author MZI1SK
 *
 */
public class SynchImages extends EShopInitialization {

	private MStore store=null;
	private Map<Integer ,Integer > mapProducts = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapImages = new HashMap<Integer ,Integer >();
	private Timestamp lastsuccess;
	
	private static final int IMG_WIDTH = 1280;
	private static final int IMG_HEIGHT = 960;

	/**
	 * 
	 */
	public SynchImages(MStore store,PrestashopSync process) {
		
		super(store,process);
		this.store=store;

		if (this.ws==null)
		    this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);

	}
	
	public void run() {
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductImage, MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, null);

		
		mapProducts=getMapProducts();
		
		long start = Calendar.getInstance().getTimeInMillis();
		
		List<MProduct> products=getProductsToSynch( synch );
		for (MProduct product:products){
			System.out.println("Product started "+product.getValue());
		
			if (mapProducts.get(product.get_ID())==null)
					continue;
			
			long id = mapProducts.get(product.get_ID());
			
			System.out.printf("Product Images sync starting "+product.getValue()+" ID: %d\n", id);
			
		
	        String completeUrl =  store.getURL()+"/api/images/products/"+ String.valueOf(toIntExact(id));
	        // check if image exists 
	        try {
		        HttpGet httpget = new HttpGet(completeUrl);
		        HashMap<String,Object> resoult = ws.executeRequest(httpget);
		        System.out.println("checking exists image : "+completeUrl);
		        if ((int) resoult.get("status_code")==200) {
			        try {
			        	System.out.println("Image exists deleting");
			        	HttpDelete httpdelete = new HttpDelete(completeUrl);
			        	ws.executeRequest(httpdelete);
			        } catch (PrestaShopWebserviceException e) {
						System.out.println("product image "+completeUrl+ " NOT deleted ");
						try {
							TimeUnit.SECONDS.sleep(180);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}
		        }
	        } catch(PrestaShopWebserviceException e) {
	        	
	        }
    		// add image 
			if (product.getAD_Image_ID()>0) {
				MImage image=new MImage(Env.getCtx(),product.getAD_Image_ID(),null);
				if (image.getUpdated().after(lastsuccess))
					addProductImages(image, product, toIntExact(id));
			}
			if (product.getAD_Image2_ID()>0) {
				MImage image=new MImage(Env.getCtx(),product.getAD_Image2_ID(),null);
				if (image.getUpdated().after(lastsuccess))
					addProductImages(image, product, toIntExact(id));
			}
			if (product.getAD_Image3_ID()>0) {
				MImage image=new MImage(Env.getCtx(),product.getAD_Image3_ID(),null);
				if (image.getUpdated().after(lastsuccess))
					addProductImages(image, product, toIntExact(id));
			}
			if (product.getAD_Image4_ID()>0) {
				MImage image=new MImage(Env.getCtx(),product.getAD_Image4_ID(),null);
				if (image.getUpdated().after(lastsuccess))
					addProductImages(image, product, toIntExact(id));
			}
			if (product.getAD_Image5_ID()>0) {
				MImage image=new MImage(Env.getCtx(),product.getAD_Image5_ID(),null);
				if (image.getUpdated().after(lastsuccess))
					addProductImages(image, product, toIntExact(id));
			}
			if (product.getAD_Image6_ID()>0) {
				MImage image=new MImage(Env.getCtx(),product.getAD_Image6_ID(),null);
				if (image.getUpdated().after(lastsuccess))
					addProductImages(image, product, toIntExact(id));
			}
			
		}
		
		long end = Calendar.getInstance().getTimeInMillis();
		System.out.printf("Products - (POST) - Execution time: %.2f seconds\n" , (end - start)/1000.0);
		
		MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, null);
		synchsucces.setAD_Org_ID(store.getAD_Org_ID());
		synchsucces.setSuccess(true);
		synchsucces.setExternalSystem("PS");
		synchsucces.setTimestampLocal(new Timestamp(start));
		synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductImage);
		synchsucces.setW_Store_ID(store.get_ID());
		synchsucces.saveEx();
		setMapImages(mapImages);

	}
	
	public void initialize() {

			HashMap<String,Object> product_images = new HashMap<String, Object>();
			product_images.put("resource", "images/products/");
			Document ws_product_images;
			
			try {
				ws_product_images = ws.get(product_images);
			 
			System.out.println(ws.DocumentToString(ws_product_images));
			if (ws_product_images.getElementsByTagName("image").getLength()>0){
				for (int i=0;i<ws_product_images.getElementsByTagName("image").getLength();i++){
					Node image= ws_product_images.getElementsByTagName("image").item(i);
					NamedNodeMap attrs = image.getAttributes();
					String image_id = attrs.getNamedItem("id").getNodeValue();
					HashMap<String,Object> product_image_todel = new HashMap<String, Object>();
					product_image_todel.put("resource", "images/products/"+image_id);
					
					Document ws_images;
					//HashMap<String,Object> images = new HashMap<String, Object>();
					
					int maxAttempts= 3;
					for (int count = 0; count < maxAttempts; count++) {
						try {
						PSWebServiceClient ws_imgs = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
						ws_images= ws_imgs.get(product_image_todel);
						
						if (ws_images.getElementsByTagName("declination").getLength()>0){
							//System.out.println(ws_imgs.DocumentToString(ws_images));
							for (int j=0;j<ws_images.getElementsByTagName("declination").getLength();j++){
								Node imagtodel= ws_images.getElementsByTagName("declination").item(j);
								if (imagtodel!=null){
									NamedNodeMap attrsdel = imagtodel.getAttributes();
									String imagetodel_id = attrsdel.getNamedItem("id").getNodeValue();
									HashMap<String,Object> image_todel = new HashMap<String, Object>();
									image_todel.put("resource", "images/products/"+image_id);
									image_todel.put("id",imagetodel_id);
									PSWebServiceClient ws_del;
									try {
									ws_del =new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
									Boolean deleted= ws_del.delete(image_todel);
									System.out.println("product image id."+imagetodel_id+ " deleted "+deleted);
									} catch (PrestaShopWebserviceException e) {
										System.out.println("product image id."+imagetodel_id+ " NOT deleted ");
										j--;
										try {
											TimeUnit.SECONDS.sleep(180);
										} catch (InterruptedException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
									}
								}
							}
						}
						count = maxAttempts;
						} catch (PrestaShopWebserviceException e) {
							System.out.println("Getting images for product  "+image_id+ " Error ");
							try {
								TimeUnit.SECONDS.sleep(180);
							} catch (InterruptedException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
				}
	        
	        };   

			DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MImage.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_ProductImage+"' AND W_Store_ID="+store.get_ID(),null);
			
			} catch (PrestaShopWebserviceException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}   
	}

	
	private List<MProduct> getProductsToSynch( MMZI_ES_Synchronizations synch) {
		
		if ( synch==null)
			lastsuccess= new Timestamp(0);
		else 
			lastsuccess=synch.getTimestampLocal();	
		
		List<MProduct> products = new Query(Env.getCtx(), MProduct.Table_Name,
				" m_product_id in (select m_product_id from mzi_productimagestosync im where im.ad_client_id=? and im.updated>=?) "
						+ "	 AND M_Product_ID in( select ci.record_id from  C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id "
						+ " where cl.C_Vocabulary_ID in (select C_Vocabulary_ID from W_StoreVocabulary where w_store_id=?)) ",
				null)
				.setParameters(Env.getAD_Client_ID(Env.getCtx()), lastsuccess,store.getW_Store_ID())
				.list();
	
		return products;
	}
	
	private PSWebServiceClient ws;	
	
	private void addProductImages(MImage image,MProduct product,int ps_productId ){
		
		
		 if (image.getData()==null) 
			 return ;
		 
		 try {
			 
			 // check size of a image if high do resize
			 ByteArrayOutputStream os = new ByteArrayOutputStream();
			 Image img=image.getImage();
			 if (img==null) 
				 return ;
			 
			 BufferedImage bi=PSWebServiceClient.toBufferedImage(img);
			 process.ProcessStatusUpdate(product.getName()+" "+image.getName()+" image width:"+img.getWidth(null)+"  image height:"+ img.getHeight(null));	
			 if ( img.getWidth(null)>1280){
				 //System.out.println("image width:"+img.getWidth(null)+"  image height:"+ img.getHeight(null));
				 int type = bi.getType() == 0? BufferedImage.TYPE_INT_ARGB : bi.getType();
				 bi= resizeImageWithHint(bi,type);
			 }
			 
			 ImageIO.write(bi,"png", os); 
			 os.flush();
			 InputStream fis = new ByteArrayInputStream(os.toByteArray());
			
			 // synchronize
			 this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
			 Document ws_image = ws.addImgProduct(fis, ps_productId,image.getName(),"/api/images/products/",false);
			 if (ws_image!=null) {
				 int id = Integer.valueOf(PSWebServiceClient.getCharacterDataFromElement((Element) ws_image.getElementsByTagName("id").item(0)));
			  // add to mapping table
				X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
				mapping.setAD_Table_ID(MImage.Table_ID);
				mapping.setEshop_Ref_ID(toIntExact(id) );
				mapping.setRecord_ID(image.get_ID());
				mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
				mapping.setW_Store_ID(store.get_ID());
				mapping.saveEx();
				mapImages.put(product.get_ID(), toIntExact(id));
			 }
		} catch ( IOException e) {
			// TODO Auto-generated catch block
		 	// e.printStackTrace();
			System.out.println(e.getMessage());
		}
	 
    }
	


	private static BufferedImage resizeImageWithHint(BufferedImage originalImage, int type){

		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
		RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		RenderingHints.VALUE_ANTIALIAS_ON);

		return resizedImage;
	    }
	

}
