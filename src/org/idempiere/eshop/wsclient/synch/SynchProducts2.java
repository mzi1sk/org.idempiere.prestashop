/**
 * 
 */
package org.idempiere.eshop.wsclient.synch;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;
import static java.lang.Math.toIntExact;

import org.compiere.model.MAttributeSetInstance;
import org.compiere.model.MClassification;
import org.compiere.model.MMZI_ES_Synchronizations;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPrice;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MStore;
import org.compiere.model.MTax;
import org.compiere.model.MTaxCategory;
import org.compiere.model.Query;
import org.compiere.model.X_M_RelatedProduct;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.idempiere.eshop.model.X_MZI_EShop_Mapping;
import org.idempiere.eshop.wsclient.process.PrestashopSync;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.edgaragg.pshop4j.model.Limit;
import com.edgaragg.pshop4j.model.Sort;
import com.edgaragg.pshop4j.modeling.PrestaShopMapperResponse;
import com.edgaragg.pshop4j.pojos.PrestaShopPojoEntity;
import com.edgaragg.pshop4j.pojos.entities.Product;
import com.edgaragg.pshop4j.pojos.list.Products;
import org.apache.commons.lang.StringEscapeUtils;
/**
 * @author Edgar Gonzalez
 *
 */
public class SynchProducts2 extends EShopInitialization {

	private MStore store=null;
	private Map<Integer ,Integer > mapProducts = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapStorages = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapCombinations = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapAttributes = new HashMap<Integer ,Integer >();
	private Map<Integer ,Integer > mapAttributeValues = new HashMap<Integer ,Integer >();
	
	private Timestamp lastsuccess;
	
	/**
	 * 
	 */
	public SynchProducts2(MStore store,PrestashopSync process) {
		
		super(store,process);
		this.store=store;
	}
	
	public String run() {
		MMZI_ES_Synchronizations synch = MMZI_ES_Synchronizations.getLastSuccess(Env.getCtx(),
				MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product, MMZI_ES_Synchronizations.EXTERNALSYSTEM_PrestaShop , store.getW_Store_ID(), 0, null);

		
		String errorproducts="";
		mapProducts=getMapProducts();
		mapStorages=getMapStorages();
		mapCombinations=getMapCombinations();
		mapAttributes = getMapAttributes();
		mapAttributeValues =getMapAttributeValues(true);
		long start = Calendar.getInstance().getTimeInMillis();
		
		if (this.ws==null)
		    this.ws = new PSWebServiceClient(store.getURL(),store.getWStoreUserPW(),false);
		
		List<MProduct> products=getProductsToSynch( synch );
		
		for (MProduct product:products){
			process.ProcessStatusUpdate("Product started "+product.getValue());
//			if (product.get_ID()>0)
//				continue;
			int countRelated=DB.getSQLValueEx(null, "select count(*) from M_RelatedProduct where M_Product_ID = "+product.getM_Product_ID());
			if (countRelated>0) {
				doCombination(product);
				continue;
			}
			
			try {
				HashMap<String,Object> getSchemaOpt = new HashMap<String, Object>();
				MProductPrice pp=MProductPrice.getLatest(Env.getCtx(),store.getM_PriceList_ID(), product.getM_Product_ID(), null);
				if (pp==null||pp.getM_PriceList_Version()==null) {
					errorproducts=errorproducts+"\n Product:"+product.getValue()+" missing pricelist";
					continue;
				}
				BigDecimal price=Env.ZERO;
				if (pp.getM_PriceList_Version().getM_PriceList().isTaxIncluded())
				{
					MTax stdTax = new MTax (Env.getCtx(), 
							((MTaxCategory) product.getC_TaxCategory()).getDefaultTax().getC_Tax_ID(), 
							null);
					BigDecimal taxStdAmt = stdTax.calculateTax(pp.getPriceList(), true, 2);
					price=pp.getPriceList().subtract(taxStdAmt);
				}
				else
				price=pp.getPriceList();
				
				
		
		
				getSchemaOpt.put("url", store.getURL()+"/api/products?schema=blank");       
				Document schema = ws.get(getSchemaOpt);     
		        schema.getElementsByTagName("id_category_default").item(0).setTextContent(String.valueOf(getCategorieDefault(product,store)));
		        if (getMapImages().get(product.get_ID())!=null){
		        	schema.getElementsByTagName("id_default_image").item(0).setTextContent(String.valueOf(getMapImages().get(product.get_ID())));
		        	Element id_default_image = (Element) schema.getElementsByTagName("id_default_image").item(0);
		        	id_default_image.setAttribute("xlink:href",store.getURL()+"/images/products/"+mapProducts.get(product.get_ID()+"/"+getMapImages().get(product.get_ID()))) ;        
		        }
		        schema.getElementsByTagName("price").item(0).setTextContent(price.toString()); 
		        schema.getElementsByTagName("available_for_order").item(0).setTextContent("1");
	        	schema.getElementsByTagName("quantity").item(0).setNodeValue(product.getStorage().toString()); //toto nieje stav na sklad ale hladaj stock_available
		        schema.getElementsByTagName("show_price").item(0).setTextContent("1"); 
		        schema.getElementsByTagName("state").item(0).setTextContent("1"); 
		        schema.getElementsByTagName("indexed").item(0).setTextContent("1");
		        schema.getElementsByTagName("id_tax_rules_group").item(0).setTextContent("1");  
				if (!product.isActive())
					schema.getElementsByTagName("active").item(0).setTextContent("0");  //True = 1  False = 2 
				else 
			        schema.getElementsByTagName("active").item(0).setTextContent("1"); 
	
				List<MClassification> categories=getCategoriesByProduct(product.get_ID());
				
				for (MClassification cat:categories){
					Element category = schema.createElement("category");
			        Element catId = schema.createElement("id");
			        catId.setTextContent(String.valueOf(getMapCategories().get(cat.getC_Classification_ID())));
			        category.appendChild(catId);
			        schema.getElementsByTagName("categories").item(0).appendChild(category);
				}

				productLanguageSection(schema,product,"1","En_US",0);
				productLanguageSection(schema,product,"2","SK_sk",1);
		      
				HashMap<String,Object> productOpt = new HashMap<String, Object>();
		        productOpt.put("resource", "products");
		        
				boolean doPut=false;
				
						
					if (!mapProducts.isEmpty() && mapProducts.get(product.get_ID())!=null)
						doPut=true;
					
					long id = 0;
					
					if (doPut){
				         schema.getElementsByTagName("id").item(0).setTextContent(Integer.toString(mapProducts.get(product.get_ID())));
						 productOpt.put("id", mapProducts.get(product.get_ID()));
						 productOpt.put("putXml", ws.DocumentToString(schema));
				        Document ws_product = ws.edit(productOpt);     
				        //System.out.println(ws.DocumentToString(ws_product));
				        if (ws_product.getElementsByTagName("stock_available").getLength()>0){
					        Node storage= ws_product.getElementsByTagName("stock_available").item(0);
					        int storage_id=Integer.valueOf(storage.getFirstChild().getNextSibling().getFirstChild().getNodeValue());
					        updateStorageMap(product, storage_id);
				        }
				        id = mapProducts.get(product.get_ID());
				        
					}	
					else
					{
						try {
						productOpt.put("postXml", ws.DocumentToString(schema));
				        Document ws_product = ws.add(productOpt);     
				      //  System.out.println(ws.DocumentToString(ws_product));
				        id = Integer.valueOf(PSWebServiceClient.getCharacterDataFromElement((Element) ws_product.getElementsByTagName("id").item(0)));
				        if (ws_product.getElementsByTagName("stock_available").getLength()>0){
						        Node storage= ws_product.getElementsByTagName("stock_available").item(0);
						        int storage_id=Integer.valueOf(storage.getFirstChild().getNextSibling().getFirstChild().getNodeValue());
						        updateStorageMap(product, storage_id);
				        	}    
						} 
						catch (Exception e) {
							// TODO: handle exception
							process.ProcessStatusUpdate("Product "+product.getValue()+" error uploading ");
						}
					}
					
					process.ProcessStatusUpdate("Product updated "+product.getValue()+" ID: "+ id);
						
					if (!doPut){
					// add to mapping table
						X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
						mapping.setAD_Table_ID(MProduct.Table_ID);
						mapping.setEshop_Ref_ID(toIntExact(id) );
						mapping.setRecord_ID(product.get_ID());
						mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
						mapping.setW_Store_ID(store.get_ID());
						mapping.saveEx();
						mapProducts.put(product.get_ID(), toIntExact(id));
					}
		
			} catch (PrestaShopWebserviceException | TransformerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return e.getMessage();
			}
			
		
		} // <- FOR products
		
		long end = Calendar.getInstance().getTimeInMillis();
		process.ProcessStatusUpdate("Products - (POST) - Execution time: "+(end - start)/1000.0 +" seconds"  );
		
		MMZI_ES_Synchronizations synchsucces=new MMZI_ES_Synchronizations(Env.getCtx(), 0, null);
		synchsucces.setAD_Org_ID(store.getAD_Org_ID());
		synchsucces.setSuccess(true);
		synchsucces.setExternalSystem("PS");
		synchsucces.setTimestampLocal(new Timestamp(start));
		synchsucces.setSynchronizationType(MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product);
		synchsucces.setW_Store_ID(store.get_ID());
		synchsucces.saveEx();
		setMapProducts(mapProducts);
		
		if (errorproducts.equals("")) return "OK";
		
		return errorproducts;
	}
	
	private boolean doCombination(MProduct product) {
		
		 if (mapAttributeValues.isEmpty())
			 return false;
 
		 long id=0;
		 
		 if (!mapCombinations.isEmpty() && mapCombinations.get(product.get_ID())!=null) {        	 
         }

		X_M_RelatedProduct related = new Query(Env.getCtx(), X_M_RelatedProduct.Table_Name, "M_Product_ID=? ", null)
					.setParameters(new Object[]{product.get_ID()})
					.setOnlyActiveRecords(true)
					.first();
		 
		if (related==null || mapProducts.get(related.getRelatedProduct_ID())==null )
			 return false;
		
		MProduct parent=new MProduct (Env.getCtx(),related.getRelatedProduct_ID(),null); 
		String ps_id_parent=Integer.toString(mapProducts.get(related.getRelatedProduct_ID()));
		 
		try {

			 	HashMap<String,Object> getSchemaOpt = new HashMap<String, Object>();
				getSchemaOpt.put("url", store.getURL()+"/api/combinations?schema=blank");       
				Document schemaCombination = ws.get(getSchemaOpt);     
			//	schemaCombination.getElementsByTagName("ean13").item(0).setTextContent(product.getUPC());
				schemaCombination.getElementsByTagName("quantity").item(0).setTextContent("0");
				schemaCombination.getElementsByTagName("minimal_quantity").item(0).setTextContent("1");
				
				Element id_product=  (Element) schemaCombination.getElementsByTagName("id_product").item(0);
				id_product.setAttribute("xlink:href", store.getURL()+"/api/products/"+ps_id_parent);
				id_product.appendChild(schemaCombination.createCDATASection(ps_id_parent));
				
			//	NodeList  associations = (NodeList) schemaCombination.getElementsByTagName("associations");
				NodeList  product_option_values  = (NodeList) schemaCombination.getElementsByTagName("product_option_values");
				Element e_product_option_values = (Element) product_option_values.item(0);
				e_product_option_values.setAttribute("nodeType", "product_option_value");
				e_product_option_values.setAttribute("api", "product_option_values");
		        MAttributeSetInstance asi=new MAttributeSetInstance(Env.getCtx(),product.getM_AttributeSetInstance_ID(),null);
		        MAttributeSetInstance parent_asi=new MAttributeSetInstance(Env.getCtx(), parent.getM_AttributeSetInstance_ID(),null);
		        
		        Element product_option_value=  (Element) schemaCombination.getElementsByTagName("product_option_value").item(0);
	        	
		        int counter=0;
		        for (org.compiere.model.MAttributeInstance m_attribute:asi.getMAttributeInstances() ) {
		        	
		        	org.compiere.model.MAttributeInstance parentAttribute = (m_attribute.getM_Attribute()!=null? parent_asi.getAttributeInstance(m_attribute.getM_Attribute().getName()):null);
		        	
		        	// if parent has value for this attribute use parent value
		        	if (parentAttribute!=null && parentAttribute.getM_AttributeValue_ID()!=0) {
		        		if (counter==0) {
		        			product_option_value.setAttribute("xlink:href", store.getURL()+"/api/product_option_values/"+mapAttributeValues.get(parentAttribute.getM_AttributeValue_ID()));
		        			Element id_product_option_value = getDirectChild( product_option_value,"id");
		        			id_product_option_value.appendChild(schemaCombination.createCDATASection(Integer.toString(mapAttributeValues.get(parentAttribute.getM_AttributeValue_ID()))));

		        		} else {
		        			Element product_option_value_copy=(Element)product_option_value.cloneNode(true);
		        			product_option_value_copy.setAttribute("xlink:href", store.getURL()+"/api/product_option_values/"+mapAttributeValues.get(parentAttribute.getM_AttributeValue_ID()));
		        			e_product_option_values.appendChild(product_option_value_copy);
		        			
		        			Element id_product_option_value = getDirectChild( product_option_value_copy,"id");
		        			id_product_option_value.replaceChild(schemaCombination.createCDATASection(Integer.toString(mapAttributeValues.get(parentAttribute.getM_AttributeValue_ID()))), id_product_option_value.getChildNodes().item(0))	;
		        			
		        			
		        			//id_product_option_value.appendChild(schemaCombination.createCDATASection(Integer.toString(mapAttributeValues.get(m_attribute.getM_AttributeValue_ID())));
		        		}
		        		counter++;
		        		
		        	}
		        	// get aattribute from child product 
		        	else if (m_attribute.getM_AttributeValue_ID()>0 &&  mapAttributeValues.get(m_attribute.getM_AttributeValue_ID())!=null) {
		        		if (counter==0) {
		        			product_option_value.setAttribute("xlink:href", store.getURL()+"/api/product_option_values/"+mapAttributeValues.get(m_attribute.getM_AttributeValue_ID()));
		        			Element id_product_option_value = getDirectChild( product_option_value,"id");
		        			id_product_option_value.appendChild(schemaCombination.createCDATASection(Integer.toString(mapAttributeValues.get(m_attribute.getM_AttributeValue_ID()))));

		        		} else {
		        			Element product_option_value_copy=(Element)product_option_value.cloneNode(true);
		        			product_option_value_copy.setAttribute("xlink:href", store.getURL()+"/api/product_option_values/"+mapAttributeValues.get(m_attribute.getM_AttributeValue_ID()));
		        			e_product_option_values.appendChild(product_option_value_copy);
		        			
		        			Element id_product_option_value = getDirectChild( product_option_value_copy,"id");
		        			id_product_option_value.replaceChild(schemaCombination.createCDATASection(Integer.toString(mapAttributeValues.get(m_attribute.getM_AttributeValue_ID()))), id_product_option_value.getChildNodes().item(0))	;
		        			
		        			
		        			//id_product_option_value.appendChild(schemaCombination.createCDATASection(Integer.toString(mapAttributeValues.get(m_attribute.getM_AttributeValue_ID())));
		        		}
		        		counter++;
			        }
		        }
		        
		   //     attribute.appendChild(product_option_values);
		        Element images=  (Element) schemaCombination.getElementsByTagName("images").item(0);     
		        images.setAttribute("nodeType", "image");
		        images.setAttribute("api", "images/products");
		   		getSchemaOpt.put("postXml", ws.DocumentToString(schemaCombination));
				Document ws_attr = ws.add(getSchemaOpt);     
				id = Integer.valueOf(PSWebServiceClient.getCharacterDataFromElement((Element) ws_attr.getElementsByTagName("id").item(0)));
				process.ProcessStatusUpdate("combination added " + product.getName());
		        
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
		mapping.setAD_Table_ID(X_M_RelatedProduct.Table_ID);
		mapping.setEshop_Ref_ID(toIntExact(id) );
		mapping.setRecord_ID(product.get_ID());
		mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
		mapping.setW_Store_ID(store.get_ID());
		mapping.saveEx();
		mapCombinations.put(product.get_ID(), toIntExact(id));

		return true;
		
	}

	private void productLanguageSection(Document schema, MProduct product,String langID, String lang, int lang_id) {
		Element link_rewrite = (Element) schema.getElementsByTagName("link_rewrite").item(0).getChildNodes().item(lang_id);
        link_rewrite.appendChild(schema.createCDATASection(product.get_Translation("Name",lang ).toLowerCase().replaceAll("[^A-Za-z0-9\\s]", "").replace(" ","-")));
        link_rewrite.setAttribute("id", langID);
        link_rewrite.setAttribute("xlink:href",store.getURL()+"/api/languages/"+langID);           
        
        if (product.getName()!=null){
	        Element meta_title = (Element) schema.getElementsByTagName("meta_title").item(0).getChildNodes().item(lang_id);
	        meta_title.appendChild(schema.createCDATASection(StringEscapeUtils.escapeXml(product.get_Translation("Name",lang ))));
	        meta_title.setAttribute("id", langID);
	        meta_title.setAttribute("xlink:href",store.getURL()+"/api/languages/"+langID);    

	        Element name = (Element) schema.getElementsByTagName("name").item(0).getChildNodes().item(lang_id);
	        name.appendChild(schema.createCDATASection(product.get_Translation("Name",lang )));
	        name.setAttribute("id", langID);
	        name.setAttribute("xlink:href", store.getURL() +"/api/languages/"+langID);

        }
        
        
        if (product.getDescription()!=null){
	        Element description = (Element) schema.getElementsByTagName("description_short").item(0).getChildNodes().item(lang_id);
	        description.appendChild(schema.createCDATASection(product.get_Translation("Description",lang )));
	        description.setAttribute("id", langID);
	        description.setAttribute("xlink:href",store.getURL()+"/api/languages/"+langID);

	        Element meta_description = (Element) schema.getElementsByTagName("meta_description").item(0).getChildNodes().item(lang_id);
	        meta_description.appendChild(schema.createCDATASection(product.get_Translation("Description",lang )));
	        meta_description.setAttribute("id", langID);
	        meta_description.setAttribute("xlink:href",store.getURL()+"/api/languages/"+langID);   


        }
    
        if (product.getHelp()!=null){
	        Element description_short = (Element) schema.getElementsByTagName("description").item(0).getChildNodes().item(lang_id);
	        description_short.appendChild(schema.createCDATASection(product.get_Translation("Help",lang )));
	        description_short.setAttribute("id", langID);
	        description_short.setAttribute("xlink:href",store.getURL()+"/api/languages/"+langID);  
        }
        
        Element meta_keywords = (Element) schema.getElementsByTagName("meta_keywords").item(0).getChildNodes().item(lang_id);
        meta_keywords.appendChild(schema.createCDATASection("name"));
        meta_keywords.setAttribute("id", langID);
        meta_keywords.setAttribute("xlink:href",store.getURL()+"/api/languages/"+langID);    
        
	}

	private List<MClassification> getCategoriesByProduct(int m_product_id ) {

		
		List<MClassification> categories=new Query(Env.getCtx(), MClassification.Table_Name, " c_classification_id in (select ci.c_classification_id from c_classificationinstance ci "
				+ " join  c_classification cl on cl.c_classification_id=ci.c_classification_id where ci.record_id=? AND  cl.C_Vocabulary_ID  in (select C_Vocabulary_ID from W_StoreVocabulary where w_store_id=?) ) ", null)
				.setParameters(m_product_id, store.getW_Store_ID())
				.setClient_ID()
				.setOrderBy(MClassification.COLUMNNAME_C_Classification_ID)
				.list();
		
		List<MClassification> final_categories= new ArrayList<MClassification>(categories);
		for (MClassification cat:categories){
			MClassification category = new MClassification ( Env.getCtx() ,cat.getC_ParentClassification_ID(),null);
			if (final_categories.contains(category))
				continue;
			addParentCat(final_categories, category );
		}
		return final_categories;
	}	
	
	private void addParentCat(List<MClassification> final_categories, MClassification cat ){
		if (cat.get_ID()>0)
			final_categories.add(cat);
		MClassification category = new MClassification ( Env.getCtx() ,cat.getC_ParentClassification_ID(),null);
		if (final_categories.contains(category))
			return;
		if (cat.getC_ParentClassification_ID()==0)
			return;
		
		addParentCat(final_categories, category );
	}
	
	public void initialize() {
			try {
				List<String> fields = Collections.emptyList();
				PrestaShopMapperResponse<Products> result = this.getMapper().list(Products.class, fields, this.getFilters(), Sort.EMPTY_SORT, Limit.EMPTY_LIMIT);
				Products resource = result.getResource();
				for (int i=0;i<resource.size();i++){
					Product rst = resource.get(i);
					PrestaShopPojoEntity entity=(PrestaShopPojoEntity) rst;
					this.getMapper().delete(entity);
					process.ProcessStatusUpdate("Product deleted ID: " + rst.getId());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MProduct.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_Product+"' AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_ES_Synchronizations WHERE SYNCHRONIZATIONTYPE='"+ MMZI_ES_Synchronizations.SYNCHRONIZATIONTYPE_StockCurrent+"' AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+MStorageOnHand.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
			DB.executeUpdate("DELETE FROM MZI_EShop_Mapping WHERE AD_Table_ID="+X_M_RelatedProduct.Table_ID+" AND W_Store_ID="+store.get_ID(),null);
			
	}

	private List<MProduct> getProductsToSynch( MMZI_ES_Synchronizations synch) {
		
		if ( synch==null)
			lastsuccess= new Timestamp(0);
		else 
			lastsuccess=synch.getTimestampLocal();	
		
		List<MProduct> products=new Query(Env.getCtx(), MProduct.Table_Name, " ( Updated>? "
				+ " or (SELECT max(pp.Updated) FROM m_productprice pp join m_pricelist_version pv on pp.m_pricelist_version_id=pp.m_pricelist_version_id"
				+ " WHERE pv.m_pricelist_id=? AND m_product.m_product_id=pp.m_product_id  )>?"
				+ " or (SELECT max(ci.Updated) FROM C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id " + 
				"	 where cl.C_Vocabulary_ID in (select C_Vocabulary_ID from W_StoreVocabulary where w_store_id=?) AND ci.record_id=M_Product_ID )>? )"
				+ " AND M_Product_ID in "
				+ " ( select ci.record_id from  C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id " 
				+ "   where cl.C_Vocabulary_ID in (select C_Vocabulary_ID from W_StoreVocabulary where w_store_id=?)"
				+ " Union all"
				+ "   select mr.m_product_id from  C_ClassificationInstance ci join c_classification cl on cl.c_classification_id=ci.c_classification_id "
				+ "			join m_relatedproduct mr on mr.RelatedProduct_ID  = ci.record_id " + 
				"	 where cl.C_Vocabulary_ID in (select C_Vocabulary_ID from W_StoreVocabulary where w_store_id=?)) ", null)
				.setParameters(lastsuccess,store.getM_PriceList_ID(),lastsuccess,store.getW_Store_ID(),lastsuccess, store.getW_Store_ID(),store.getW_Store_ID())
				.setClient_ID()
				.setOrderBy("value asc ")
				//.setOrderBy("issummary desc ")
				.list();
	
		return products;
	}

	private void updateStorageMap(MProduct product, int storage_id){
		 if (!mapStorages.containsKey(product.get_ID())&& storage_id>0 ){
				X_MZI_EShop_Mapping mapping= new X_MZI_EShop_Mapping(Env.getCtx(),0,null);
				mapping.setAD_Table_ID(MStorageOnHand.Table_ID);
				mapping.setEshop_Ref_ID(storage_id );
				mapping.setRecord_ID(product.get_ID());
				mapping.setExternalSystem(X_MZI_EShop_Mapping.EXTERNALSYSTEM_PrestaShop);
				mapping.setW_Store_ID( store.get_ID());
				mapping.saveEx();
				mapStorages.put(product.get_ID(), storage_id);
			 
		 }
	 
   }
	private PSWebServiceClient ws;	
	


	public static Element getDirectChild(Element parent, String name)
	{
	    for(Node child = parent.getFirstChild(); child != null; child = child.getNextSibling())
	    {
	        if(child instanceof Element && name.equals(child.getNodeName())) return (Element) child;
	    }
	    return null;
	}




	
}
